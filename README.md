# Living styleguide generator
The docsite generator transforms Markdown files found in zk's repos, styles them, moves them around, rewrites links to match, and spits out [https://styleguide.zilverenkruis.nl](https://styleguide.zilverenkruis.nl).

## Developers:
Build hugo container: `docker build -t zk/hugo .`
Start development environment: `docker-compose up -d`
url: [http://localhost:3001](http://localhost:3001)

## Install & run styleguide
You need Ruby to install & run the styleguide.
Use a tool e.g. [rbenv](https://github.com/rbenv/rbenv)

Install styleguide:
```bash
bundle install
```

Run styleguide:
```bash 
bundle exec jekyll serve
```

## Manage repo's (projects) for documentation
https://developer.atlassian.com/blog/2015/05/the-power-of-git-subtree/
`git clone --depth=1 --branch=feature/monorepo https://WibeHarteveld@bitbucket.org/WibeHarteveld/zk-styleguide.git docs-src/zk-styleguide && rm -rf !$/.git`

<!-- Add repo:\
`git subtree add --prefix docs-src/[project-name] https://bitbucket.com/repo/to/include.git master --squash` -->

Add repo subtree as a remote:\
`git remote add -f zk-styleguide https://WibeHarteveld@bitbucket.org/WibeHarteveld/zk-styleguide.git`

Add subtree repo branch:\
`git subtree add --prefix docs-src/zk-styleguide zk-styleguide master --squash`

Update subtree repo branch:\
`git subtree pull --prefix docs-src/zk-styleguide zk-styleguide master --squash`

## FileManager
[Filemanager official website](https://filebrowser.github.io/)\
[FileManager tutorial](https://www.ostechnix.com/filemanager-cross-platform-stylish-web-file-manager/)

## Jekyll
Show the gem based theme minima: `bundle show minima`
