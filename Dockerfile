FROM debian:jessie
MAINTAINER flyer090@gmail.com

# Install pygments (for syntax highlighting)
RUN apt-get -qq update \
	&& DEBIAN_FRONTEND=noninteractive apt-get -qq install -y --no-install-recommends python-pygments git ca-certificates asciidoc curl \
	&& rm -rf /var/lib/apt/lists/*

# Download and install hugo
# ENV HUGO_BINARY hugo_${HUGO_VERSION}_Linux-64bit.deb
ENV HUGO_DIR /usr/share/hugo
ENV HUGO_SOURCE_DIR $HUGO_DIR/src
ENV PORT 1313
ENV BASE_URL http://localhost

RUN mkdir /tmp/hugo \
	&& LATEST_RELEASE=$(curl -L -s -H 'Accept: application/json' https://github.com/gohugoio/hugo/releases/latest) \
	&& LATEST_VERSION=$(echo $LATEST_RELEASE | sed -e 's/.*"tag_name":"\([^"]*\)".*/\1/' | sed 's/^v//') \
	# run LATEST_VERSION=$(echo $LATEST_VERSION | sed 's/^v//') \
	&& HUGO_BINARY=$(echo hugo_${LATEST_VERSION}_Linux-64bit.deb) \
	&& curl -SL https://github.com/spf13/hugo/releases/download/v${LATEST_VERSION}/${HUGO_BINARY} -o /tmp/hugo/${HUGO_BINARY} \
	&& dpkg -i /tmp/hugo/${HUGO_BINARY}

# Create working directory
RUN mkdir ${HUGO_DIR}
WORKDIR $HUGO_DIR

# Expose default hugo port
EXPOSE $PORT

# Automatically build site
# ONBUILD ADD site/ $HUGO_DIR
ONBUILD RUN hugo -s ${HUGO_SOURCE_DIR}

# By default, serve site
CMD hugo server -s ${HUGO_SOURCE_DIR} -p ${PORT} -b ${BASE_URL} --bind=0.0.0.0

