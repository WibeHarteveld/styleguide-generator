const sass = require('node-sass');
const sassExtract = require('sass-extract');
const writeYaml = require('write-yaml');
const fs = require('fs');

function getSassVariables(fileSrc) {
  return sassExtract.render({
    file: fileSrc
  })
}

function writeJsonToYaml(jsonData, fileOutput) {
  writeYaml(fileOutput, jsonData, function(err) {
    if (!err) {
      console.log('file written');
    } else {
      console.log(err);
    }
  });
}

function sassToYaml(fileSrc, fileOutput) {
  if (fs.existsSync(fileSrc)) {
    getSassVariables(fileSrc).then(rendered => {
      let colorsMap = new Map();

      Object.keys(rendered.vars.global).map((key, value) => {
        if (!key.includes('-list')) {
          colorsMap.set(key, rendered.vars.global[key]);
        }
      });

      const colorsJson = strMapToObj(colorsMap);

      writeJsonToYaml(colorsJson, fileOutput);
    });
    // return getSassVariables(fileSrc);
  }
}

function mapToJson(map) {
  return JSON.stringify([...map]);
}

function strMapToObj(strMap) {
  let obj = Object.create(null);
  for (let [k,v] of strMap) {
      // We don’t escape the key '__proto__'
      // which can cause problems on older engines
      obj[k] = v;
  }
  return obj;
}
function objToStrMap(obj) {
  let strMap = new Map();
  for (let k of Object.keys(obj)) {
      strMap.set(k, obj[k]);
  }
  return strMap;
}

module.exports = {
  sassToYaml,
};
