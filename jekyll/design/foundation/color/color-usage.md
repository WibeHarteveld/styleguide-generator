---
title: Color usage
---

This is the color usage page.

## This is a test image

![This is alt text](https://media.istockphoto.com/photos/colorful-bokeh-background-picture-id525961208?k=6&m=525961208&s=612x612&w=0&h=dnkdheTZgaJwfg7cO9MRzQB58O_ekO6YjlW44XaNSvs= "Logo title text")

## Another one

![This is alt text](/assets/img/test.jpg "Logo title text")
