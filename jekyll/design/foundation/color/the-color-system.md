---
title: The color system
---

This is the color system page.

<!-- {% include color.html name="Color gray" hex="#333" rgb="51 51 51" %}
{% include color.html name="Color blue" hex="#3657c1" rgb="54 87 193" %}
{% include color.html name="Color red" hex="#c31c1c" rgb="195 28 28" %} -->

## Zilveren Kruis color palette
{% include color-palette.html %}
