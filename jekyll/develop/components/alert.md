---
title: Alert
lead: "Provide contextual feedback messages for typical user actions with the handful of available and flexible alert messages."
---

{% include component-example.html %}

<span class="lead">Contents:</span>
* Usage
* Anatomy
* Hierarchy and placement
* Theming
* Specs
* Implementation

## Usage
Text

## Anatomy
Text

## Behavior
Text

## Hierarchy and placement
Text

## Theming
Text

Example:
{% include component-example.html story="with some emoji" %}

## Specs
Text

## Implementation
[Web](http://localhost:9009/)